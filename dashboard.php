
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Inventaris - Login Form</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
</head>

<body>

    <!--main content start-->
 
      <section class="wrapper site-min-height">
        <div class="row">
          <div class="col-lg-12">

            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<h1><p align="center"><font color="black">Peminjaman Inventaris Sarana dan Prasana di <?php echo $data['nama_sekolah']; ?></font></p></h1>
              <!-- end custombox -->
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
<h1><p align="center">
<a href="index.php" button type="button" class="btn btn-warning"><i class="fa fa-off"></i>Masuk</button></a>
<a href="daftar.php" button type="button" class="btn btn-primary"><i class="fa fa-off"></i>Daftar</button></a>
</p></h1>
              <!-- end custombox -->
            </div>
            <!-- end col-4 -->
          </div>
            <!-- end col-4 -->
          <!--  /col-lg-12 -->
        </div>
          <!--  /col-lg-12 -->

        <!--  /row -->
      </section>
      <!-- /wrapper -->
    </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="lib/jquery.backstretch.min.js"></script>
  <script>
    $.backstretch("img/bg.jpg", {
      speed: 500
    });
  </script>
</body>
</html>
