<?php 
include "cek.php";
error_reporting(0);
?>
<?php 
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Pinjam Barang | Operator - Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datetimepicker/datertimepicker.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
   <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li>
            <a class="active" href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-mail-forward">&nbsp;</i>Form Pinjam Barang </h3>
        <div class="row mt">
          <!--  DATE PICKERS -->
          <div class="col-lg-12">
            <div class="site-min-height content-panel">
              <section id="unseen">
              <form class="form-horizontal tasi-form" method="POST" action="">
                <div class="form-group">
                  <label class="control-label col-md-4"></label>
                  <div class="col-md-3 col-xs-11">
                    <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="2016-01-01" class="input-append date dpYears">
              <select name="jml" id="jml"  class="form-control">
											<option>---Jumlah Pinjam---</option>
							<?php for ($jml=1; $jml <= 5; $jml++){ ?>
							<option value="<?php echo $jml; ?>"><?php echo $jml;?></option>
							<?php } ?>
                </select>
                    </div>
                  </div>
                </div>
                  <div class="btn-middle">
                    <div class="col-lg-offset-5 col-7">
                      &nbsp;<button class="btn btn-large btn-warning" type="submit" name="Tambah">Filter</button>

                    </div>
                  </div>
              </form>
			  <hr>
	
<?php
 
include "koneksi.php";
$query = "SELECT max(kd_peminjaman) as maxKode FROM peminjaman";
$hasil = mysql_query($query);
$data = mysql_fetch_array($hasil);
$kodePeminjaman = $data['maxKode'];
$noUrut = (int) substr($kodePeminjaman, 4, 4);
$noUrut++;
$char = "PJM";
$kodePeminjaman = $char . sprintf("%04s", $noUrut);
?>
<?php
 
include "koneksi.php";
$auto=mysql_query("select * from peminjaman order by id_peminjaman desc limit 1");
$no=mysql_fetch_array($auto);
$angka=$no['id_peminjaman']+1;
?>			  
              <form action="proses_peminjaman.php" method="post" class="form-horizontal style-form">
					<input name="tgl_pinjam" type="hidden" class="input-xlarge datepicker" id="date01">
					<input name="tgl_kembali" type="hidden" class="input-xlarge datepicker" id="date01">

                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Kode Peminjaman	</label>
                    <div class="col-lg-6">
					  <input name="kd_peminjaman" value="<?php echo $kodePeminjaman;?>" readonly="" class=" form-control" id="cname" name="nama" minlength="2" type="text" autofocus required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2"> Nama Pegawai	</label>
                    <div class="col-lg-6">
                                            <select name="id_pegawai" class="form-control">                                     
											<option>---Pilih---</option>
											<?php ;
											include "koneksi.php";
											$result = mysql_query("SELECT * FROM  pegawai");
											while($row = mysql_fetch_assoc($result))
											{
												echo "<option>$row[id_pegawai] - $row[nama_pegawai]</option>";
											}
											?>
                                            </select>
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Status Peminjaman	</label>
                    <div class="col-lg-6">
						<select name="status_peminjaman" class="form-control" >
										<option></option>
										<option>Sedang Dipinjam</option>

										
						</select>
                    </div>
                  </div>
			  <?php if(isset($_POST['Tambah'])) { ?>
<?php for ($text=0; $text < $_POST["jml"]; $text++){ ?>
		<div class="form-group row">
			<label class="col-sm-2 control-label" ></label>
			<div class="col-sm-3">
						<select name="id_inventaris[]" id="id_inventaris[]" class="form-control" >
										<option>-Pilih Barang-</option>
										<?php
										include "koneksi.php";
										$status='P';
										//display values in combobox/dropdown
										$result = mysql_query("SELECT id_inventaris,nama,jml from inventaris ");
										while($row = mysql_fetch_assoc($result))
										{
										echo "<option>$row[id_inventaris].$row[nama] ($row[jml])</option>";
										} 
											?>
										
						</select>	
			</div>
			<div class="col-sm-3">
						<input name="jml_pinjam[]" id="jml[]" type="number" id="only-number" class="form-control" placeholder="Masukan Jumlah barang" autocomplete="off" maxlength="11" required="">
					    <input name="id_detail_pinjam[]" type="hidden" value="<?php echo $angka;?>" id="inputError">
						<input name="status[]" type="hidden" value="<?php echo $status; ?>" class="form-control">
						<input name="jml" type="hidden" value="<?php echo $data['jml']; ?>" class="form-control">
			</div>
		</div>
					 <?php
										}
										?>					  
                  <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-8">
                      <button class="btn btn-theme02 btn-large" type="submit">Proses</button>
                    </div>
                  </div>
										<?php
										}
										?> 				  
              </form>
              </section>
            </div>
            <!-- /content-panel -->
          </div>         
        </div>
        <!-- /row -->
        <!-- DATE TIME PICKERS -->
   
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>

        <a href="pinjam_barang.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="lib/advanced-form-components.js"></script>

</body>

</html>
