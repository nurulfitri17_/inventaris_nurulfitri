<?php 
include "cek.php";
error_reporting(0);
?>
<?php 
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Detail Pinjam | Operator - Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datetimepicker/datertimepicker.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
   <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li> 
          <li>
            <a class="active" href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
   <!--main content start-->

    <section id="main-content">
      <section class="wrapper site-min-height">
<h3><i class="fa fa-info"></i>&nbsp;Detail Peminjaman</h3>
        <div class="col-lg-12 mt">
          <div class="row content-panel">
            <div class="col-lg-10 col-lg-offset-1">
              <div class="invoice-body">
                <br>
                <br>
                <br>
                <div class="row">
									<?php
										include "koneksi.php";
										$id_peminjaman=$_GET['id_peminjaman'];
										$select=mysql_query("select*from peminjaman a 
																						join pegawai b on b.id_pegawai=a.id_pegawai where id_peminjaman='$id_peminjaman'
																					");
										$data=mysql_fetch_array($select);
										
										$date = $data['tgl_pinjam'];
										$tanggal = date('d-F-Y', strtotime($date));
						
									?>
<div class="col-lg-12">									
                  <div class="col-md-8">
<table>
<tr><td><b>Kode Peminjam</b>&nbsp;</td>
	<td>:</td>
	<td><?php echo $data ['kd_peminjaman']; ?></td>
</tr>									
<tr><td><b>Tanggal Pinjam</b>&nbsp;</td>
	<td>:</td>
	<td><?php echo $tanggal; ?></td>
</tr>		
<tr><td><b>Status Peminjaman</b>&nbsp;</td>
	<td>:</td>
	<td><?php echo $data ['status_peminjaman']; ?></td>
</tr>
</table>
                  </div>
                  <!-- /col-md-9 -->
                  <div class="col-md-4">
<table>											
<tr><td><b>Nama Pegawai</b>&nbsp;</td>
	<td>:</td>
	<td><?php echo $data ['nama_pegawai']; ?></td>
</tr>

<tr><td><b>NIP</b></td>
	<td>:</td>
	<td><?php echo $data ['nip']; ?></td>
</tr>
<tr><td><b>Alamat</b></td>
	<td>:</td>
	<td><?php echo $data ['alamat']; ?></td>
</tr>
</table>										
                  <!-- /invoice-body -->
                </div>
</div>
<br><br><br>
<hr>

                <!-- /col-lg-10 -->
				<div class="col-lg-12">
                <table class="table">
                  <thead>
                    <tr>
					  <th style="width:20px">Pilih</th>
                      <th style="width:130px" class="text-center">ID Detail Pinjam</th>
                      <th style="width:115px" class="text-center">Kode Inventaris</th>
                      <th class="text-left">Nama Barang</th>
					  <th style="width:130px" class="text-right">Status</th>
                      <th style="width:130px" class="text-right">Jumlah</th>
                    </tr>
                  </thead>
<?php
include "koneksi.php";

$id_peminjaman=$_GET['id_peminjaman'];
$select=mysql_query("select * from peminjaman left join detail_pinjam on detail_pinjam.id_detail_pinjam=peminjaman.id_peminjaman
											  left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris
  where id_peminjaman='$id_peminjaman'");
while($data=mysql_fetch_array($select)){

?>
                  <tbody>
                    <tr>
					  <td class="text-center"><input type="checkbox" id="check-item" name="pilih[]" value="<?php echo $data['id']?>">
                      <td class="text-center"><?php echo $data['id_detail_pinjam']; ?></td>
                      <td class="text-center"><?php echo $data ['kode_inventaris' ]; ?></td>
                      <td><?php echo $data ['nama' ]; ?></td>
					  <td class="text-right"><?php echo $data ['status' ]; ?></td>
                      <td class="text-right"><?php echo $data ['jml_pinjam' ]; ?> Buah</td>
                    </tr>
                  </tbody>

<?php } ?>
                </table>

				</div>
                <br>
                <br>
              </div>
			  </div>
			</div>
			 </div>
             </div>
              <!--/col-lg-12 mt -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>
        <a href="detail_pinjam.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="lib/advanced-form-components.js"></script>
<script> $(document).ready(function(){ 
// Ketika halaman sudah siap (sudah selesai di load) 
$("#check-all").click(function(){ 
// Ketika user men-cek checkbox all 
if($(this).is(":checked")) 
// Jika checkbox all diceklis 
$(".check-item").prop("checked", true); 
// ceklis semua checkbox siswa dengan class "check-item" else 
// Jika checkbox all tidak diceklis 
$(".check-item").prop("checked", false); 
// un-ceklis semua checkbox siswa dengan class "check-item" 
}); 
$("#btn-delete").click(function(){ 
// Ketika user mengklik tombol delete 
var confirm = window.confirm("Apakah Anda yakin ingin menghapus data-data ini?"); 
// Buat sebuah alert konfirmasi 
if(confirm) 
// Jika user mengklik tombol "Ok" 
$("#form-delete").submit(); 
// Submit form 
}); 
}); 
</script>
</body>

</html>
