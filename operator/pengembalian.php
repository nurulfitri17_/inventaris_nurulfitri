<?php 
include "cek.php";
error_reporting(0);
?>
<?php 
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>			
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Pengembalian | Operator - Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
   <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li> 
          <li>
            <a href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a  class="active" href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-mail-reply"></i>&nbsp;Data Pengembalian</h3>
        <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <form class="form-horizontal tasi-form" method="GRT" action="">
                <div class="form-group">
                  <label class="control-label col-md-4"></label>
                  <div class="col-md-3 col-xs-11">
                    <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="2016-01-01" class="input-append date dpYears">
					<label>Kembalikan Inventaris yang Sedang Dipinjam</label>
              <select name="id_peminjaman" class="form-control">
                        <?php
                        include "koneksi.php";
                                //display values in combobox/dropdown
                        $result = mysql_query("SELECT id_peminjaman,kd_peminjaman from peminjaman where status_peminjaman='Sedang Dipinjam' ");
                        while($row = mysql_fetch_assoc($result))
                        {
                            echo "<option value='$row[id_peminjaman]'>$row[kd_peminjaman]</option>";
                        } 
                        ?>
                </select>
                    </div>
                  </div>
                </div>
                  <div class="btn-middle">
                    <div class="col-lg-offset-5 col-7">
                      &nbsp;<button class="btn btn-large btn-warning" type="submit" name="Filter">Cari</button>

                    </div>
                  </div>
              </form>
			  <br>
			  <?php if(isset($_GET['Filter'])) { ?>			  
              <div class="invoice-body">
                <div class="row">
									<?php
										include "koneksi.php";
										$id_peminjaman=$_GET['id_peminjaman'];
										$select=mysql_query("select*from peminjaman a 
																						join pegawai b on b.id_pegawai=a.id_pegawai where id_peminjaman='$id_peminjaman'
																					");
										$data=mysql_fetch_array($select);
										
										$date = $data['tgl_pinjam'];
										$tanggal = date('d-F-Y H:i:s', strtotime($date));
						
									?>
<div class="col-lg-12">									
                  <div class="col-md-8">
<table>
<tr><td><b>Kode Peminjam</b>&nbsp;</td>
	<td>:</td>
	<td><?php echo $data ['kd_peminjaman']; ?></td>
</tr>									
<tr><td><b>Tanggal Pinjam</b>&nbsp;</td>
	<td>:</td>
	<td><?php echo $tanggal; ?></td>
</tr>		
<tr><td><b>Status Peminjaman</b>&nbsp;</td>
	<td>:</td>
	<td><?php echo $data ['status_peminjaman']; ?></td>
</tr>
</table>
                  </div>
                  <!-- /col-md-9 -->
                  <div class="col-md-4">
<table>											
<tr><td><b>Nama Pegawai</b>&nbsp;</td>
	<td>:</td>
	<td><?php echo $data ['nama_pegawai']; ?></td>
</tr>

<tr><td><b>NIP</b></td>
	<td>:</td>
	<td><?php echo $data ['nip']; ?></td>
</tr>
<tr><td><b>Alamat</b></td>
	<td>:</td>
	<td><?php echo $data ['alamat']; ?></td>
</tr>
</table>										
                  <!-- /invoice-body -->
                </div>
</div>
<br><br><br>
<hr>
<form action="proses_kembali.php" method="post">
                <!-- /col-lg-10 -->
				<div class="col-lg-12">
                <table class="table">
                  <thead>
                    <tr>
					  <th style="width:20px">Pilih</th>
                      <th style="width:130px" class="text-center">ID Detail Pinjam</th>
                      <th style="width:115px" class="text-center">Kode Inventaris</th>
                      <th class="text-left">Nama Barang</th>
                      <th style="width:130px" class="text-right">Jumlah</th>
                    </tr>
                  </thead>
<?php
include "koneksi.php";

$id_peminjaman=$_GET['id_peminjaman'];
$select=mysql_query("select * from peminjaman i left join detail_pinjam p on p.id_detail_pinjam=i.id_peminjaman
												left join inventaris v on p.id_inventaris=v.id_inventaris
												where id_peminjaman='$id_peminjaman' AND status ='P'");
while($data=mysql_fetch_array($select)){

?>
                    <input name="id_peminjaman" type="hidden" class="form-control " placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_detail_pinjam[]" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_inventaris[]" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>.<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="">
                     <input type="hidden" name="id[]" value="<?php echo $data['id']?>" >  
                        <input type="hidden" name="tgl_pinjam" value="<?php echo $data['tgl_pinjam']?>" >
                        <input type="hidden" name="tgl_kembali" >
                        <input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai']?>" >
                        <input type="hidden" name="jml_pinjam[]" value="<?php echo $data['jml_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah">
					<tbody>
                    <tr>
					  <td class="text-center"><input type="checkbox" id="check-item" name="kembali[]" value="<?php echo $data['id']?>">
                      <td class="text-center"><?php echo $data['id_detail_pinjam']; ?></td>
                      <td class="text-center"><?php echo $data ['kode_inventaris' ]; ?></td>
                      <td><?php echo $data ['nama']; ?></td>
                      <td class="text-right"><?php echo $data ['jml_pinjam' ]; ?> Buah</td>
                    </tr>
<?php } ?>
                    <tr>
                      <td colspan="3" rowspan="5">
<td class="text-right"><strong>                  </strong></td>
                        <td class="text-right">
						<br>
                 <input type="hidden" name="status_peminjaman" value="<?php echo $data['status_peminjaman']?>" >
				<input type="hidden" name="status[]" value="<?php echo $data['status']?>" >
				<button type="sumbit" class="btn btn-warning pull-right"><i class="fa fa-mail-reply"></i>&nbsp;Kembalikan </button></td>
                    </tr>
                  </tbody>
                </table>



				</div>
				</form>
                <br>
                <br>
              </div>
			  </div>
													  
			  <?php
			  }
			?>			  
			  <hr>
			  <br>
			  <h3>Data Inventaris yang Sudah Dikembalikan</h3>
              <table cellpadding="0" cellspacing="0" border="0" class=" table table-bordered" id="hidden-table-info">
			  <thead>
                  <tr class="success">
                    <th>No</th>
                    <th>Tanggal Pinjam</th>
					<th>Tanggal Kembali</th>
                    <th>Status Peminjaman</th>
					<th>Pegawai</th>
                  </tr>
                </thead>
                <tbody>
									<?php
										include "koneksi.php";
										$no=1;
										$select=mysql_query("SELECT * from peminjaman JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai where status_peminjaman='Sudah Dikembalikan'");
										while($data=mysql_fetch_array($select))
										{	
										$date = $data['tgl_pinjam'];
										$tanggal_p = date('d-F-Y H:i:s', strtotime($date));
										$date = $data['tgl_kembali'];
										$tanggal_k = date('d-F-Y H:i:s', strtotime($date));										
					
									?>
                  <tr class="info">
                    <td><?php echo $no++; ?></td>
					<td><?php echo $tanggal_p; ?></td>
					<td><?php echo $tanggal_k;  ?></td>
					<td><button type="button" class="btn btn-info btn-xs"><?php echo $data ['status_peminjaman' ]; ?></button></td>
					<td><?php echo $data ['nama_pegawai' ]; ?></td>					

                  </tr>

 										<?php
										}
										?>		             
                </tbody>
              </table>
              </section>
            <!-- /content-panel -->
          </div>
          <!-- /col-lg-4 -->
        </div>
        <!-- /row -->
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>
        <a href="pengembalian.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
  


      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
