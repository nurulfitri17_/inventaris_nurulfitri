<?php 
include "cek.php";
error_reporting(0);
?>
<?php 
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>			
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Data Pegawai | Admin - Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpgs" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
   <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a class="active" href="user.php">
              <i class="fa fa-user"></i>
              <span>User</span>
            </a>
          </li>
          <li>
            <a class="active" href="pengaturan.php">
              <i class="fa fa-gear"></i>
              <span>Pengaturan</span>
            </a>
          </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li>
            <a  class="active" href="inventarisir.php">
              <i class="fa fa-list"></i>
              <span>Inventarisir </span>
              </a>
          </li> 
          <li>
            <a href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>
          <li>
            <a href="laporan.php">
              <i class="fa fa-info"></i>
              <span>Generate Laporan </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-table"></i>&nbsp; Data Pegawai</h3>
        <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <section id="unseen">
			  &nbsp;<a href="#" button type="button" class="btn btn-theme03" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Baru</button></a>
                  <div class="btn-group pull-right">
                    <a data-toggle="dropdown" href="#" class="btn btn-theme02 ">
                      
                      <i class="fa fa-chevron-down "></i>
					  Master Data
                      </a>
                    <ul class="dropdown-menu">
                      <li><a href="ruang.php">Ruang</a></li>
                      <li><a href="jenis.php">Jenis</a></li>
					  <li><a href="pegawai.php">Pegawai</a></li>
					  <li><a href="level.php">Level</a></li>
                    </ul>
                  </div>
				  <br><br>
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
<thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Pegawai</th>
                    <th>Nomor Induk Pegawai</th>
                    <th>Alamat Pegawai</th>
					<th>Action</th>
                  </tr>
                </thead>
                <tbody>
									<?php
										include "koneksi.php";
										$no=1;
										$select=mysql_query("select*from pegawai");
										while($data=mysql_fetch_array($select))
										{	
									?>
                  <tr>
                    <td><?php echo $no++; ?></td>
					<td><?php echo $data ['nama_pegawai' ]; ?></td>
					<td><?php echo $data ['nip' ]; ?></td>
					<td><?php echo $data ['alamat' ]; ?></td>
					<td>
					<a href="#" button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal<?php echo $data['id_pegawai']; ?>"><i class="fa fa-pencil"></i> Edit</button></a>
					<a href="hapus_pegawai.php?id_pegawai=<?php echo $data['id_pegawai']; ?>" button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Hapus</button></a>
					</td>

                  </tr>
			<div class="modal fade" id="myModal<?php echo $data['id_pegawai']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Data Pegawai</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_pegawai.php" method="get">

                        <?php
                        $id_pegawai = $data['id_pegawai']; 
                        $query_edit = mysql_query("SELECT * FROM pegawai WHERE id_pegawai='$id_pegawai'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysql_fetch_array($query_edit)) {  
                        ?>

                        <input type="hidden" name="id_pegawai" value="<?php echo $row['id_pegawai']; ?>" required>

                        <div class="form-group">
                          <label>Nama Pegawai</label>
                          <input type="text" name="nama_pegawai" class="form-control" value="<?php echo $row['nama_pegawai']; ?>" required>      
                        </div>
                        <div class="form-group">
                          <label>Nomor Induk Pegawai</label>
                          <input type="text" name="nip" class="form-control" value="<?php echo $row['nip']; ?>" required>      
                        </div>
                        <div class="form-group">
                          <label>Alamat Pegawai</label>
                          <input type="text" name="alamat" class="form-control" value="<?php echo $row['alamat']; ?>" required>      
                        </div>
                  <div class="form-group">
              
                      <button class="btn btn-theme02" type="submit">Perbarui</button>
                      <button class="btn btn-warning" type="reset">Reset</button>
                   
                  </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
										<?php
										}
										?>	              
                </tbody>
              </table>
			<div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Data Pegawai</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="simpan_pegawai.php" method="post">
                        <div class="form-group">
                          <label>Nama Pegawai</label>
                          <input type="text" name="nama_pegawai" class="form-control" placeholder="Masukkan Nama Pegawai" required autofocus>      
                        </div>
                        <div class="form-group">
                          <label>Nomor Induk Pegawai</label>
                          <input type="text" name="nip" class="form-control" placeholder="Masukkan NIP" required autofocus>      
                        </div>
                        <div class="form-group">
                          <label>Alamat Pegawai</label>
                          <input type="text" name="alamat" class="form-control" placeholder="Masukkan Alamat Pegawai" required autofocus>      
                        </div>                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-theme02">Simpan</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
    
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
              </section>
            </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-lg-4 -->
        </div>
        <!-- /row -->
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>
        <a href="pegawai.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
  


      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
