<?php
# koneksi database
mysql_connect("localhost","root","");
mysql_select_db("ujikom_nfr");

# include fdpf tergantung direktori file kamu
include "fpdf/fpdf.php";
$tgl = date('d-M-Y');
$pdf = new FPDF('P','mm','A4',array(297,210));
$pdf->Open();
$pdf->addPage();
$pdf->setAutoPageBreak(false);

# untuk menuliskan nama bulan dengan format Indonesia
$bln_list = array(
  '01' => 'Januari',
  '02' => 'Februari',
  '03' => 'Maret',
  '04' => 'April',
  '05' => 'Mei',
  '06' => 'Juni',
  '07' => 'Juli',
  '08' => 'Agustus',
  '09' => 'September',
  '10' => 'Oktober',
  '11' => 'November',
  '12' => 'Desember'
);

# header
$pdf->setFont('Arial','',12);
$pdf->Image('img/kopsurat.png',10,1,200,50);
$pdf->text(75,55,'Laporan Data Peminjaman');
$pdf->text(70,60,'SMKN 1 CIOMAS TAHUN '.date('Y'));

$yi = 70;
$ya = 70;
$pdf->setFont('Arial','',9);
$pdf->setFillColor(222,222,222);
$pdf->setXY(5,$ya);
$pdf->CELL(7,6,'No',1,0,'C',1);
$pdf->CELL(35,6,'KD Peminjaman',1,0,'C',1);
$pdf->CELL(40,6,'Tgl Pinjam',1,0,'C',1);
$pdf->CELL(40,6,'Tgl Kembali',1,0,'C',1);
$pdf->CELL(40,6,'Status Peminjaman',1,0,'C',1);
$pdf->CELL(35,6,'Pegawai',1,0,'C',1);
# menampilkan data dari database
$no = 1;
$row = 6;
				$dari = $_POST['dari'];
				$sampai = $_POST['sampai'];
$sql = mysql_query("select * from peminjaman JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai where (tgl_pinjam between '$dari' and '$sampai')");

$ya = $yi + $row;
while($data = mysql_fetch_array($sql)){

  $pdf->setXY(5,$ya);
  $pdf->setFont('arial','',9);
  $pdf->setFillColor(255,255,255);
  $pdf->cell(7,6,$no,1,0,'C',1);
  $pdf->cell(35,6,$data['kd_peminjaman'],1,0,'L',1);
  $pdf->cell(40,6,$data['tgl_pinjam'],1,0,'L',1);
  $pdf->CELL(40,6,$data['tgl_kembali'],1,0,'L',1);
  $pdf->cell(40,6,$data['status_peminjaman'],1,0,'L',1);
  $pdf->cell(35,6,$data['nama_pegawai'],1,0,'L',1);  
  $ya = $ya+$row;
  $no++;
}
$pdf->text(10, 65, 'Periode :'.$dari.' s/d '.$sampai.'',0,0,'C');
# footer
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','',9);
$pdf->SetX(120);
$pdf->MultiCell(95,10,'Bogor, '.date('d').' '.$bln_list[date('m')].' '.date('Y'),0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,0, 'KEPALA SEKOLAH SMKN 1 CIOMAS',0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,20, ' ',0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,1, 'Drs. Miswan Wahyudi, MM.',0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,1, '_________________________',0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,8, 'NIP : 196706012000031003 ',0,'C');
$pdf->Ln();
$pdf->output();
$pdf->Output('laporan-pdf-peminjaman.pdf','i'); // menampilkan di browser

?>