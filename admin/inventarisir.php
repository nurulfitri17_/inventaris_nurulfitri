<?php 
include "cek.php";
error_reporting(0);
?>
<?php 
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Data Inventaris | Admin - Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datetimepicker/datertimepicker.css" />
  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a class="active" href="user.php">
              <i class="fa fa-user"></i>
              <span>User</span>
            </a>
          </li>
          <li>
            <a class="active" href="pengaturan.php">
              <i class="fa fa-gear"></i>
              <span>Pengaturan</span>
            </a>
          </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li>
            <a  class="active" href="inventarisir.php">
              <i class="fa fa-list"></i>
              <span>Inventarisir </span>
              </a>
          </li> 
          <li>
            <a href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>
          <li>
            <a href="laporan.php">
              <i class="fa fa-info"></i>
              <span>Generate Laporan </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-table"></i>&nbsp;Data Inventaris Sekolah</h3>
        <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <section id="unseen">
			  &nbsp;<a href="tambah_barang.php" button type="button" class="btn btn-theme03"><i class="fa fa-plus"></i> Tambah Baru</button></a>                 
				 <div class="btn-group pull-right">
                    <a data-toggle="dropdown" href="#" class="btn btn-theme02 ">
                      
                      <i class="fa fa-chevron-down "></i>
					  Referensi
                      </a>
                    <ul class="dropdown-menu">
                      <li><a href="ruang.php">Ruang</a></li>
                      <li><a href="jenis.php">Jenis</a></li>
					  <li><a href="pegawai.php">Pegawai</a></li>
					  <li><a href="level.php">Level</a></li>
                    </ul>
                  </div>
				  <br>
              <div class="btn-group pull-right">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				   <img class="img" src="img/print.jpg"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                      <li><a href="#" data-toggle="modal" data-target="#myModal1"><img class="img" src="img/excel.jpg"> To Excel</a></li>
                      <li><a href="#" data-toggle="modal" data-target="#myModal"><img class="img" src="img/pdf.jpg"></i> To PDF</a></li>
                </ul>
              </div>
				  <br><br>
              <table cellpadding="0" cellspacing="0" border="0" class=" table table-bordered" id="hidden-table-info">
				  <thead>
                  <tr class="success">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Kondisi</th>
                    <th>Keterangan</th>
					<th>Jumlah</th>
					<th>Jenis</th>
					<th>Tanggal Register</th>
					<th>Ruang</th>
					<th>Kode Inventaris</th>
					<th>Action</th>
                  </tr>
                </thead>
                <tbody>
									<?php
										include "koneksi.php";
										$no=1;
										$select=mysql_query("select * from inventaris a 
																						join jenis b on b.id_jenis=a.id_jenis
																						join ruang c on c.id_ruang=a.id_ruang

																			ORDER BY kode_inventaris");
										while($data=mysql_fetch_array($select))
										{	
										$date = $data['tgl_register'];
										$tanggal = date('d-F-Y', strtotime($date));
									?>
                  <tr class="danger">
                    <td><?php echo $no++; ?></td>
					<td><?php echo $data ['nama' ]; ?></td>
					<td><?php echo $data ['kondisi' ]; ?></td>
					<td><?php echo $data ['ket' ]; ?></td>
					<td><?php echo $data ['jml' ]; ?></td>
					<td><?php echo $data ['nama_jenis' ]; ?></td>
					<td><?php echo $tanggal; ?></td>
					<td><?php echo $data ['nama_ruang' ]; ?></td>
					<td><?php echo $data ['kode_inventaris' ]; ?></td>
		
					<td>
					<a href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>" button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button></a>
					<a href="hapus_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>" button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Hapus</button></a>
					</td>

                  </tr>
										<?php
										}
										?>	              
                </tbody>
              </table>
			<div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cetak PDF Inventaris</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="laporan_pdf_inventaris.php" method="post" target="_blank">
						<div class="form-group row">
							<label class="col-sm-3 control-label" >Sampai Tanggal :</label>
								<div class="col-sm-9">
									<input name="dari"  size="16" type="date" value="">				
								</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label" >Sampai Tanggal :</label>
								<div class="col-sm-9">
									<input name="sampai" size="16" type="date" value="">
				
								</div>
						</div>	
		
                        <div class="modal-footer">  
                          <button type="submit" name="submit" class="btn btn-theme02"><i class="fa fa-print"></i>Cetak</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
    
                      </form>			
                  </div>
                </div>
                
              </div>
            </div>
			<div class="modal fade" id="myModal1" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cetak Excel Inventaris</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="laporan_excel_inventaris.php" method="post" target="_blank">
						<div class="form-group row">
							<label class="col-sm-3 control-label" >Sampai Tanggal :</label>
								<div class="col-sm-9">
									<input name="dari"  size="16" type="date" value="">				
								</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label" >Sampai Tanggal :</label>
								<div class="col-sm-9">
									<input name="sampai" size="16" type="date" value="">
				
								</div>
						</div>	
		
                        <div class="modal-footer">  
                          <button type="submit" name="submit" class="btn btn-theme02"><i class="fa fa-print"></i>Cetak</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
    
                      </form>			
                  </div>
                </div>
                
              </div>
            </div>
         </section>
          <!-- /col-lg-4 -->
        </div>
        <!-- /row -->
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>
        <a href="inventaris.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="lib/advanced-form-components.js"></script>
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
  


      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>

