<?php
include "cek.php";
error_reporting(0);
?>
<?php
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashboard | Admin - Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a class="active" href="user.php">
              <i class="fa fa-user"></i>
              <span>User</span>
            </a>
          </li>
          <li>
            <a class="active" href="pengaturan.php">
              <i class="fa fa-gear"></i>
              <span>Pengaturan</span>
            </a>
          </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a class="active" href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li>
            <a href="inventarisir.php">
              <i class="fa fa-list"></i>
              <span>Inventarisir </span>
              </a>
          </li> 
          <li>
            <a href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>
          <li>
            <a href="laporan.php">
              <i class="fa fa-info"></i>
              <span>Generate Laporan </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row">
          <div class="col-lg-12">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="custom-box">
                <div class="servicetitle">
                  <h1>Login Berhasil !!!</h1>
				  <h4>Anda berhasil login dengan detail sebagai berikut:</h4>
				  <p>Username : <?php echo $_SESSION ['username']; ?></p>

				  <p>Level : <?php echo $_SESSION ['nama_level']; ?></p>
				  <p><a href="#" button type="button" class="btn btn-theme03" data-toggle="modal" data-target="#myModal"><i class="fa fa-off"></i>Log Out</button></a></p>
                  <hr>
                </div>

                </div>
              <!-- end custombox -->
            </div>
            <!-- end col-4 -->
          </div>
          <!--  /col-lg-12 -->
        </div>
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Anda Yakin Ingin Keluar?</h4>
      </div>
      <div class="pull-right modal-body">
       <button type="button" class="btn btn-warning" data-dismiss="modal">Tidak, Terima Kasih</button>
	     <a href="logout.php" button type="button" class="btn btn-theme04">Ya, Keluar</button></a>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
            </div>

        <!--  /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>
        <a href="index.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

</body>

</html>
