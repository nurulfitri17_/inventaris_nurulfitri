<?php 
include "cek.php";
error_reporting(0);
?>
<?php 
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>			
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Pengaturan | Admin - Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datetimepicker/datertimepicker.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
   <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a class="active" href="user.php">
              <i class="fa fa-user"></i>
              <span>User</span>
            </a>
          </li>
          <li>
            <a class="active" href="pengaturan.php">
              <i class="fa fa-gear"></i>
              <span>Pengaturan</span>
            </a>
          </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li>
            <a href="inventarisir.php">
              <i class="fa fa-list"></i>
              <span>Inventarisir </span>
              </a>
          </li> 
          <li>
            <a href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>
          <li>
            <a href="laporan.php">
              <i class="fa fa-info"></i>
              <span>Generate Laporan </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-4">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Edit
                  </h4>
              </header>
            <section class="panel">
            <div class="form-panel">
              <form action="update_pengaturan.php?id=1" method="post" class="form-horizontal style-form">
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-3">Nama Sekolah</label>
                    <div class="col-lg-8">
                      <textarea class=" form-control" id="cname" name="nama_sekolah" rows="2" minlength="2" type="text" required /><?php echo $data['nama_sekolah'];?></textarea>
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-3">Alamat Sekolah</label>
                    <div class="col-lg-8">
                      <textarea class=" form-control" id="cname" name="alamat_sekolah" rows="3" minlength="2" type="text" required /><?php echo $data['alamat_sekolah'];?></textarea>
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-3">Telp Sekolah	</label>
                    <div class="col-lg-8">
                      <input class=" form-control" id="cname" name="telp" value="<?php echo $data['telp'];?>" minlength="2" type="text" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-3">Kode Pos Sekolah	</label>
                    <div class="col-lg-8">
                      <input class=" form-control" id="cname" name="kodepos" value="<?php echo $data['kodepos'];?>" minlength="2" type="text" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-3">Email Sekolah	</label>
                    <div class="col-lg-8">
                      <input class=" form-control" id="cname" name="email" value="<?php echo $data['email'];?>" minlength="2" type="text" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-3">Website Sekolah	</label>
                    <div class="col-lg-8">
                      <input class=" form-control" id="cname" name="web" value="<?php echo $data['web'];?>" minlength="2" type="text" required />
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-8">
                      <button class="btn btn-theme02" type="submit">Simpan</button>
                      <button class="btn btn-warning" type="reset">Reset</button>
                    </div>
                  </div>				  
              </form>
            </div>
            <!-- /form-panel -->
            </section>
          </div>
          <div class="col-sm-8">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Detail Sekolah
                  </h4>
              </header>
              <div class="panel-body minimal">
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
									<?php
										include "koneksi.php";
										$no=1;
										$select=mysql_query("select*from pengaturan where id='1'");
										while($data=mysql_fetch_array($select))
										{	
						
									?>
                      <tr class="read">
                        <td class="inbox-small-cells"><i class="fa fa-arrow-right"></i></td>
                        <td class="view-message  dont-show"><b>Nama Sekolah</b></td>
                        <td class="view-message "><?php echo $data ['nama_sekolah' ]; ?></td>
                      </tr>
                      <tr class="read">
                        <td class="inbox-small-cells"><i class="fa fa-arrow-right"></i></td>
                        <td class="view-message dont-show"><b>Alamat Sekolah</b></td>
                        <td class="view-message"><?php echo $data ['alamat_sekolah' ]; ?></td>
                      </tr>
                      <tr class="read">
                        <td class="inbox-small-cells"><i class="fa fa-arrow-right"></i></td>
                        <td class="view-message dont-show"><b>Telp Sekolah</b></td>
                        <td class="view-message"><?php echo $data ['telp' ]; ?></td>
                      </tr>
                      <tr class="read">
                        <td class="inbox-small-cells"><i class="fa fa-arrow-right"></i></td>
                        <td class="view-message dont-show"><b>Kode Pos Sekolah</b></td>
                        <td class="view-message"><?php echo $data ['kodepos' ]; ?></td>
                      </tr>
                      <tr class="read">
                        <td class="inbox-small-cells"><i class="fa fa-arrow-right"></i></td>
                        <td class="view-message dont-show"><b>Email Sekolah</b></td>
                        <td class="view-message"><?php echo $data ['email' ]; ?></td>
                      </tr>
                      <tr class="read">
                        <td class="inbox-small-cells"><i class="fa fa-arrow-right"></i></td>
                        <td class="view-message dont-show"><b>Website Sekolah</b></td>
                        <td class="view-message"><?php echo $data ['web' ]; ?></td>
                      </tr>
										<?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>
        <a href="pengaturan.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="lib/advanced-form-components.js"></script>

</body>

</html>
