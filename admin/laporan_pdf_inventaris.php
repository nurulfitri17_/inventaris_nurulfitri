<?php
# koneksi database
mysql_connect("localhost","root","");
mysql_select_db("ujikom_nfr");

# include fdpf tergantung direktori file kamu
include "fpdf/fpdf.php";
$tgl = date('d-M-Y');
$pdf = new FPDF('P','mm','A4',array(297,210));
$pdf->Open();
$pdf->addPage();
$pdf->setAutoPageBreak(false);

# untuk menuliskan nama bulan dengan format Indonesia
$bln_list = array(
  '01' => 'Januari',
  '02' => 'Februari',
  '03' => 'Maret',
  '04' => 'April',
  '05' => 'Mei',
  '06' => 'Juni',
  '07' => 'Juli',
  '08' => 'Agustus',
  '09' => 'September',
  '10' => 'Oktober',
  '11' => 'November',
  '12' => 'Desember'
);

# header
$pdf->setFont('Arial','',12);
$pdf->Image('img/kopsurat.png',10,1,200,50);
$pdf->text(75,55,'Laporan Data Inventaris');
$pdf->text(70,60,'SMKN 1 CIOMAS TAHUN '.date('Y'));

$yi = 70;
$ya = 70;
$pdf->setFont('Arial','',9);
$pdf->setFillColor(222,222,222);
$pdf->setXY(5,$ya);
$pdf->CELL(7,6,'No',1,0,'C',1);
$pdf->CELL(35,6,'Nama Barang',1,0,'C',1);
$pdf->CELL(20,6,'Kondisi',1,0,'C',1);
$pdf->CELL(25,6,'Keterangan',1,0,'C',1);
$pdf->CELL(15,6,'Jumlah',1,0,'C',1);
$pdf->CELL(20,6,'Jenis',1,0,'C',1);
$pdf->CELL(25,6,'Tgl Register',1,0,'C',1);
$pdf->CELL(30,6,'Ruang',1,0,'C',1);
$pdf->CELL(25,6,'KD Inventaris',1,0,'C',1);
# menampilkan data dari database
$no = 1;
$row = 6;
				$dari = $_POST['dari'];
				$sampai = $_POST['sampai'];
$sql = mysql_query("select * from inventaris a 
												join jenis b on b.id_jenis=a.id_jenis
												join ruang c on c.id_ruang=a.id_ruang  where (tgl_register between '$dari' and '$sampai')");

$ya = $yi + $row;
while($data = mysql_fetch_array($sql)){

  $pdf->setXY(5,$ya);
  $pdf->setFont('arial','',9);
  $pdf->setFillColor(255,255,255);
  $pdf->cell(7,6,$no,1,0,'C',1);
  $pdf->cell(35,6,$data['nama'],1,0,'L',1);
  $pdf->cell(20,6,$data['kondisi'],1,0,'L',1);
  $pdf->CELL(25,6,$data['ket'],1,0,'L',1);
  $pdf->cell(15,6,$data['jml'],1,0,'L',1);
  $pdf->cell(20,6,$data['nama_jenis'],1,0,'L',1); 
  $pdf->cell(25,6,$data['tgl_register'],1,0,'L',1);
  $pdf->cell(30,6,$data['nama_ruang'],1,0,'L',1);
  $pdf->cell(25,6,$data['kode_inventaris'],1,0,'L',1);  
  $ya = $ya+$row;
  $no++;
}
$pdf->text(10, 65, 'Periode :'.$dari.' s/d '.$sampai.'',0,0,'C');
# footer
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','',9);
$pdf->SetX(120);
$pdf->MultiCell(95,10,'Bogor, '.date('d').' '.$bln_list[date('m')].' '.date('Y'),0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,0, 'KEPALA SEKOLAH SMKN 1 CIOMAS',0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,20, ' ',0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,1, 'Drs. Miswan Wahyudi, MM.',0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,1, '_________________________',0,'C');
$pdf->SetX(120);
$pdf->MultiCell(95,8, 'NIP : 196706012000031003 ',0,'C');
$pdf->Ln();
$pdf->output();


?>