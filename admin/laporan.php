<?php 
include "cek.php";
error_reporting(0);
?>
<?php 
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Generate Laporan | Admin - Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datetimepicker/datertimepicker.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
   <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a class="active" href="user.php">
              <i class="fa fa-user"></i>
              <span>User</span>
            </a>
          </li>
          <li>
            <a class="active" href="pengaturan.php">
              <i class="fa fa-gear"></i>
              <span>Pengaturan</span>
            </a>
          </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li>
            <a href="inventarisir.php">
              <i class="fa fa-list"></i>
              <span>Inventarisir </span>
              </a>
          </li> 
          <li>
            <a href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>
          <li>
            <a  class="active" href="laporan.php">
              <i class="fa fa-info"></i>
              <span>Generate Laporan </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-plus-table">&nbsp;</i>Laporan</h3>
        <div class="row mt">
          <!--  DATE PICKERS -->
          <div class="col-lg-12">
            <div class="content-panel">
              <section id="unseen">
              <form class="form-horizontal tasi-form" method="get">
                <div class="form-group">
                  <label class="control-label col-md-4"></label>
                  <div class="col-md-3 col-xs-11">
                    <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="2016-01-01" class="input-append date dpYears">
                      <input name="tanggal_awal" type="text" readonly="" value="<?php echo $data['tanggal_awal']; ?>" placeholder="Dari Tanggal" size="16" class="form-control">
                      <span class="input-group-btn add-on">
                        <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4"></label>
                  <div class="col-md-3 col-xs-11">
                    <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="2016-01-01" class="input-append date dpYears">
                      <input name="tanggal_akhir" type="text" readonly="" value="" placeholder="Sampai Tanggal" size="16" class="form-control">
                      <span class="input-group-btn add-on">
                        <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                  </div>
                </div>
                  <div class="btn-middle">
                    <div class="col-lg-offset-5 col-7">
                      &nbsp;<button class="btn btn-large btn-theme02" type="submit" name="filter_data">Filter</button>

                    </div>
                  </div>
              </form>
			  <hr>  
              <!-- Single button -->
              <div class="btn-group pull-right">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				   <img class="img" src="img/print.jpg"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                      <li><a href="#" data-toggle="modal" data-target="#myModal1"><img class="img" src="img/excel.jpg"> To Excel</a></li>
                      <li><a href="#" data-toggle="modal" data-target="#myModal"><img class="img" src="img/pdf.jpg"></i> To PDF</a></li>
                </ul>
              </div>
				  <br><br><br>
              <table cellpadding="0" cellspacing="0" border="0" class=" table table-bordered" id="hidden-table-info">
<thead>
                  <tr class="success">
                    <th>No</th>
                    <th>Tanggal Pinjam</th>
					<th>Tanggal Kembali</th>
                    <th>Status Peminjaman</th>
					<th>Pegawai</th>
                  </tr>
                </thead>
                <tbody>

			<?php 
			$no = 1;
 
			if(isset($_GET['filter_data'])){
				$tanggal_awal = $_GET['tanggal_awal'];
				$tanggal_akhir = $_GET['tanggal_akhir'];

				$sql = mysql_query("select * from peminjaman JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai where tgl_pinjam between '$tanggal_awal' and '$tanggal_akhir'");
			}
			else{	
				$sql = mysql_query("select * from peminjaman JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai");
			}	
			while($data = mysql_fetch_array($sql))
			{
										$date = $data['tgl_pinjam'];
										$tanggal_p = date('d-F-Y H:i:s', strtotime($date));
									
			?>
                  <tr class="info">
                    <td><?php echo $no++; ?></td>
					<td><?php echo $tanggal_p ?></td>
					<td><?php echo $data ['tgl_kembali' ]; ?></td>
					<td><?php echo $data ['status_peminjaman' ]; ?></td>
					<td><?php echo $data ['nama_pegawai' ]; ?></td>
				  </tr>

 										<?php
										}
										?>	
										
                </tbody>
              </table>
			<div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cetak PDF Peminjaman</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="laporan_pdf_peminjaman.php" method="post" target="_blank">
						<div class="form-group row">
							<label class="col-sm-3 control-label" >Sampai Tanggal :</label>
								<div class="col-sm-9">
									<input name="dari"  size="16" type="date" value="">				
								</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label" >Sampai Tanggal :</label>
								<div class="col-sm-9">
									<input name="sampai" size="16" type="date" value="">
				
								</div>
						</div>	
		
                        <div class="modal-footer">  
                          <button type="submit" name="submit" class="btn btn-theme02"><i class="fa fa-print"></i>Cetak</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
    
                      </form>			
                  </div>
                </div>
                
              </div>
            </div>
			<div class="modal fade" id="myModal1" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cetak Excel Peminjaman</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="laporan_excel_peminjaman.php" method="post" target="_blank">
						<div class="form-group row">
							<label class="col-sm-3 control-label" >Sampai Tanggal :</label>
								<div class="col-sm-9">
									<input name="dari"  size="16" type="date" value="">				
								</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label" >Sampai Tanggal :</label>
								<div class="col-sm-9">
									<input name="sampai" size="16" type="date" value="">
				
								</div>
						</div>	
		
                        <div class="modal-footer">  
                          <button type="submit" name="submit" class="btn btn-theme02"><i class="fa fa-print"></i>Cetak</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
    
                      </form>			
                  </div>
                </div>
                
              </div>
            </div>
              </section>
            </div>
            <!-- /content-panel -->
          </div>         
        </div>
        <!-- /row -->
        <!-- DATE TIME PICKERS -->
   
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>
        <a href="laporan.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="lib/advanced-form-components.js"></script>

</body>

</html>
