<?php 
include "cek.php";
error_reporting(0);
?>
<?php 
include "cek_level.php";
?>
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Tambah Barang | Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datetimepicker/datertimepicker.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
      <a href="index.php" class="logo"><b>INVENTARIS<span>&nbsp;<?php echo $data['nama_sekolah']; ?></span></b></a>
      <!--logo end-->
      <div class="nav pull-right notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav  top-menu">

          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-gears">&nbsp; Setelan</i>
              <span class="badge bg-theme"></span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">Setelan</p>
              </li>
          <li>
            <a class="active" href="user.php">
              <i class="fa fa-user"></i>
              <span>User</span>
            </a>
          </li>
          <li>
            <a class="active" href="pengaturan.php">
              <i class="fa fa-gear"></i>
              <span>Pengaturan</span>
            </a>
          </li>
          <li>
            <a href="logout.php">
              <i class="fa fa-sign-out"></i>
              <span>Logout </span>
              </a>
          </li> 



            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->

          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <h3 class="centered"><i class="fa fa-user">&nbsp;<?php echo $_SESSION ['nama_lengkap']; ?></h3></i>
          
          <li>
            <a  href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li>
            <a class="active" href="inventarisir.php">
              <i class="fa fa-list"></i>
              <span>Inventarisir </span>
              </a>
          </li> 
          <li>
            <a href="peminjaman.php">
              <i class="fa fa-mail-forward"></i>
              <span>Peminjaman </span>
              </a>
          </li>
          <li>
            <a href="pengembalian.php">
              <i class="fa fa-mail-reply"></i>
              <span>Pengembalian </span>
              </a>
          </li>
          <li>
            <a href="laporan.php">
              <i class="fa fa-info"></i>
              <span>Generate Laporan </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-plus-square">&nbsp;</i>Form Tambah Barang</h3>
        <div class="row mt">
          <!--  DATE PICKERS -->
          <div class="col-lg-12">
            <div class="form-panel">
<?php
include "koneksi.php";

$id_inventaris=$_GET['id_inventaris'];
$select=mysql_query("select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysql_fetch_array($select);
$date = $data['tgl_register'];
$tanggal = date('d-F-Y', strtotime($date));
?>

<?php
	$select=mysql_query("select*from inventaris a 
													join jenis b on b.id_jenis=a.id_jenis
													join ruang c on c.id_ruang=a.id_ruang
													WHERE id_inventaris='$id_inventaris'
													");

													$r=mysql_fetch_array($select);
										?>
              <form action="update_inventaris.php?id_inventaris=<?php echo $id_inventaris; ?>" method="post" class="form-horizontal style-form">
		<div class="form-group row">
			<label class="col-sm-2 control-label" >Nama</label>
			<div class="col-sm-10">
				<input name="nama" class="form-control" value="<?php echo $data['nama'];?>">
				
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 control-label" >Kondisi</label>
			<div class="col-sm-10">
				<input name="kondisi" class="form-control" value="<?php echo $data['kondisi'];?>">
				
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 control-label" >Keterangan</label>
			<div class="col-sm-10">
				<input name="ket" class="form-control" value="<?php echo $data['ket'];?>">
				
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 control-label" >Jumlah</label>
			<div class="col-sm-10">
				<input name="jml" class="form-control" value="<?php echo $data['jml'];?>">
				
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 control-label" >Kode Inventaris</label>
			<div class="col-sm-10">
				<input name="kode_inventaris" class="form-control" value="<?php echo $data['kode_inventaris'];?>" readonly="">
				
			</div>
		</div>
                <div class="form-group">
                  <label class="control-label col-md-2">Tanggal Register</label>
                  <div class="col-md-3 col-xs-11">
                    <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="HH-BB-TTTT" class="input-append date dpYears">
                      <input name="tgl_register" value="<?php echo $tanggal; ?>" type="text" readonly=""  size="16" class="form-control">
                      <span class="input-group-btn add-on">
                        <button class="btn btn-theme" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                    <span class="help-block">Select date</span>
                  </div>
                </div>
		<div class="form-group row">
			<label class="col-sm-2 control-label" >Jenis</label>
			<div class="col-sm-10">
              <select name="id_jenis" class="form-control">
											<option><?php echo $r ['nama_jenis'];?></option>
											<?php 
											include "koneksi.php";
											
											$result = mysql_query("SELECT * FROM jenis ORDER BY nama_jenis");
											while($row = mysql_fetch_assoc($result))
											{
												echo "<option>$row[nama_jenis]</option>";
											}
											?>			
                </select>	
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 control-label" >Ruang</label>
			<div class="col-sm-10">
									 <select name="id_ruang"  class="form-control">
											<option><?php echo $r ['nama_ruang'];?></option>
											<?php
											include "koneksi.php";
											$result = mysql_query("SELECT * FROM ruang ORDER BY nama_ruang");
											while($row = mysql_fetch_assoc($result))
											{
												echo "<option>$row[nama_ruang]</option>";
											}
											?>
						
								  </select>
			</div>
		</div>
                 

                  <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-8">
                      <button class="btn btn-theme02" type="submit">Simpan</button>
                      <button class="btn btn-warning" type="reset">Reset</button>
                    </div>
                  </div>				  
              </form>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
        <!-- DATE TIME PICKERS -->
   
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Inventaris Sarana dan Prasarana</strong>. 2019
        </p>
        <a href="edit_inventaris.php" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="lib/advanced-form-components.js"></script>

</body>

</html>
