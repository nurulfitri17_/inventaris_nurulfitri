<?php
include "koneksi.php";

$select=mysql_query("select * from pengaturan where id='1'");
$data=mysql_fetch_array($select);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Login Form | Inventaris Sarana&Prasarana <?php echo $data['nama_sekolah']; ?></title>

  <!-- Favicons -->
  <link href="img/logo.jpg" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
</head>

<body>

  <div id="login-page">
    <div class="container">
	    <?php 
        if(isset($_GET["login_error"])){
            echo '
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Warning!</strong>  Password atau Username Salah
              </div>
			';
        }
    ?>
      <form class="form-login" action="cek_login.php" method="post">
        <h2 class="form-login-heading">LOGIN</h2>
        <div class="login-wrap">
          <input name="username" type="text" class="form-control" placeholder="Username" autofocus required>
          <br>
          <input name="password"  type="password" class="form-control" placeholder="Password" required>
		  <br>
		
		  <br>

              <button type="sumbit" class="btn btn-theme btn-lg btn-block"><i class="fa fa-lock"></i>&nbsp;Masuk </button>
            


          <hr>
          <div class="registration">
            <a class="" href="index.php">
              Back to the dasboard
              </a>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="lib/jquery.backstretch.min.js"></script>
  <script>
    $.backstretch("img/bg.jpg", {
      speed: 500
    });
  </script>
</body>
</html>
